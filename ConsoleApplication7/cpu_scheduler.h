#pragma once
#include <list>
#include "process.h"

class cpu_scheduler
{
public:
	cpu_scheduler();
	~cpu_scheduler();
private:
	list<process> list;
	float totalwaitingtime;
	int remaining;
};

