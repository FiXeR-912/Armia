#include "process.h"
#include <string>


process::process()
{
	processtime = 0.0f;
	arrivalTime = 0;
}


process::~process()
{
}


void process::SetProcessTime(float time)
{
	processtime = time;
}


float process::GetProcessTime()
{
	return processtime;
}


int process::GetArrivalTime()
{
	return arrivalTime;
}


void process::SetArrivalTime(int time)
{
	arrivalTime = time;
}


void process::SetName(string name)
{
	pName = name;
}


string process::GetName()
{
	return pName;
}
