#pragma once
#include <string>

using namespace std;

class process
{
public:
	process();
	~process();
private:
	string pName;
	float processtime;
	int arrivalTime;
public:
	void SetProcessTime(float time);
	float GetProcessTime();
	int GetArrivalTime();
	void SetArrivalTime(int time);
	void SetName(string name);
	string GetName();
};

